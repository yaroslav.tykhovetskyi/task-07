package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {
	private static String FULL_URL;

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		FULL_URL = getConnectionURL();
	}



	public List<User> findAllUsers() throws DBException {
		List<User> usersList = new ArrayList<>();

		try(Connection con = DriverManager.getConnection(FULL_URL);
			Statement stmnt = con.createStatement();
			ResultSet resultSet = stmnt.executeQuery(DBConstants.SELECT_ALL_USERS_QUERY)){

			while(resultSet.next()){
				usersList.add(prepareUser(resultSet));
			}

		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("oopsie", e);
		}
		return usersList;
	}

	public boolean insertUser(User user) throws DBException {
		try(Connection con = DriverManager.getConnection(FULL_URL);
			PreparedStatement statement = con.prepareStatement(DBConstants.INSERT_USER_QUERY, Statement.RETURN_GENERATED_KEYS)){

			int k = 0;

			statement.setString(++k, user.getLogin());
			statement.executeUpdate();

			ResultSet idSet = statement.getGeneratedKeys();

			if (idSet.next()){
				user.setId(idSet.getInt(1));
			}
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("User wasn't added", e);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			PreparedStatement statement = connection.prepareStatement(DBConstants.DELETE_USER_QUERY)){

			User forID;

			for (User temp : users){
				forID = getUser(temp.getLogin());
				int k = 0;
				statement.setInt(++k, forID.getId());
				statement.executeUpdate();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Oops", e);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User u = null;
		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 PreparedStatement statement = connection.prepareStatement(DBConstants.SELECT_USER_BY_LOGIN)
			 ) {

			int k = 0;
			statement.setString(++k, login);

			try(ResultSet results = statement.executeQuery()) {
				if (results.next()) {
					u = prepareUser(results);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Something went wrong", e);
		}

		return u;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 PreparedStatement statement = connection.prepareStatement(DBConstants.SELECT_TEAM_BY_NAME)
		) {

			int k = 0;
			statement.setString(++k, name);

			try(ResultSet results = statement.executeQuery()) {
				if (results.next()) {
					team = prepareTeam(results);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Something went wrong", e);
		}

		return team;
	}



	public List<Team> findAllTeams() throws DBException {
		List<Team> teamsList = new ArrayList<>();

		try(Connection con = DriverManager.getConnection(FULL_URL);
			Statement stmnt = con.createStatement();
			ResultSet resultSet = stmnt.executeQuery(DBConstants.SELECT_ALL_TEAMS_QUERY)){

			while(resultSet.next()){
				teamsList.add(prepareTeam(resultSet));
			}

		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("oopsie", e);
		}
		return teamsList;
	}

	public boolean insertTeam(Team team) throws DBException {
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			PreparedStatement statement = connection.prepareStatement(DBConstants.INSERT_TEAM_QUERY, Statement.RETURN_GENERATED_KEYS)){

			int k = 0;
			statement.setString(++k, team.getName());

			statement.executeUpdate();
			ResultSet generatedId = statement.getGeneratedKeys();
			if (generatedId.next()){
				team.setId(generatedId.getInt(1));
			}

		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("Team not inserted", e);
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		try{
			connection = DriverManager.getConnection(FULL_URL);
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			statement = connection.prepareStatement(DBConstants.INSERT_TEAMS_FOR_USER);

			for (Team team : teams){
				int k = 0;

				statement.setInt(++k, user.getId());
				statement.setInt(++k, team.getId());

				statement.executeUpdate();
			}

			connection.commit();
		}catch (SQLException e){
			e.printStackTrace();
			rollBack(connection);
			throw new DBException("Couldn't set teams", e);
		}finally {
			close(connection);
			close(statement);
		}
		return true;
	}



	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			PreparedStatement statement = connection.prepareStatement(DBConstants.SELECT_USER_TEAMS)){

			int k = 0;
			statement.setInt(++k, user.getId());

			ResultSet rs = statement.executeQuery();
			while(rs.next()){
				userTeams.add(prepareTeam(rs));
			}

		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("Couldn't retrieve user's teams", e);
		}

		return userTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			PreparedStatement statement = connection.prepareStatement(DBConstants.DELETE_TEAM_QUERY)){

			Team forID = getTeam(team.getName());

			int k = 0;
			statement.setInt(++k, forID.getId());

			statement.executeUpdate();
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("Error occured", e);
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			PreparedStatement statement = connection.prepareStatement(DBConstants.UPDATE_TEAM_NAME)){

			int k = 0;
			statement.setString(++k, team.getName());
			statement.setInt(++k, team.getId());

			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("oops", e);
		}
		return true;
	}

	private User prepareUser(ResultSet resultSet) throws SQLException {
		User user = new User();
		user.setId(resultSet.getInt("id"));
		user.setLogin(resultSet.getString("login"));
		return user;
	}

	private Team prepareTeam(ResultSet results) throws SQLException{
		Team team = new Team();
		team.setId(results.getInt("id"));
		team.setName(results.getString("name"));
		return team;
	}

	private static String getConnectionURL() {
		Properties props = new Properties();
		String connectionUrl = "";
		try(FileReader reader = new FileReader("app.properties")) {
			props.load(reader);
			connectionUrl = props.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return connectionUrl;
	}

	private void close(AutoCloseable closeable) throws DBException {
		if (closeable != null){
			try {
				closeable.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DBException("oops", e);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void rollBack(Connection connection) throws DBException{
		if (connection != null){
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
				throw new DBException("rollback failed", ex);
			}
		}
	}
}
