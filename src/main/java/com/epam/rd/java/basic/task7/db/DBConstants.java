package com.epam.rd.java.basic.task7.db;

public abstract class DBConstants {
//    URL
    public static final String FULL_CONNECTION_URL = "jdbc:mysql://10.7.0.9:3307/testdb?user=testuser&password=testpass";
//    SELECTS
    public static final String SELECT_ALL_USERS_QUERY = "SELECT * FROM users ORDER BY id";
    public static final String SELECT_ALL_TEAMS_QUERY = "SELECT * FROM teams ORDER BY id";
    public static final String SELECT_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
    public static final String SELECT_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ?";
    public static final String SELECT_USER_TEAMS = "SELECT * FROM teams WHERE id in(SELECT team_id FROM users_teams WHERE user_id = ?)";
//    INSERTS
    public static final String INSERT_USER_QUERY = "INSERT INTO users(id, login) VALUES (DEFAULT, ?)";
    public static final String INSERT_TEAM_QUERY = "INSERT INTO teams(id, name) VALUES (DEFAULT, ?)";
    public static final String INSERT_TEAMS_FOR_USER = "INSERT INTO users_teams(user_id, team_id) VALUES(?, ?)";
//    DELETES
    public static final String DELETE_TEAM_QUERY = "DELETE FROM teams WHERE id = ?";
    public static final String DELETE_USER_QUERY = "DELETE FROM users WHERE id = ?";
//    UPDATES
    public static final String UPDATE_TEAM_NAME = "UPDATE teams SET name = ? WHERE id = ?";
    protected DBConstants(){
//        Hiding
    }
}